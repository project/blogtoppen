
Module: Blogtoppen
Author: Lars Geisler Sehested <www.larssehested.com>


Description
===========
Adds the tracking code for Blogtoppen to your website.

Requirements
============

* Blogtoppen.dk account


Installation
============
* Copy the 'blogtoppen' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your Blogtoppen tracking key.
