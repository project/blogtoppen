<?php

/**
 * @file
 * Administrative page callbacks for the blogtoppen module.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 */
function blogtoppen_admin_settings_form(&$form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );

  $form['account']['blogtoppen_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Blogtoppen key'),
    '#default_value' => variable_get('blogtoppen_key', ''),
    '#size' => 25,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('The tracking key you received from Blogtoppen.'),
  );

  return system_settings_form($form);
}